# Package à installer sur tous les projets laravel

- nunomaduro/larastan
- laravel-self-diagnosis
- laravel-cors
- laravel-lang
- intervention/image
- slack-notification-channel
- toastr-5-laravel
- predis/predis
- horizon
- telescope
- laravel-dompdf
- larecipe
- arcanedev/log-viewer
- laravel

